+++
title = "S7.2 Graphical symbols"
+++

Graphical symbols are used only to annotate schematics and do *not* correspond to a footprint on the PCB.


. Reference Designator must be set to `#SYM` and must be *invisible*
. Symbol name must be set to _invisible_
. Graphical symbols must not contain any pins
. No pins or footprint association
. No footprint filters
. Options `exclude from BOM` and `exclude from board` must be checked

*Example:*

{{< klcimg src="S7.2_a" title="Example graphical symbol" >}}